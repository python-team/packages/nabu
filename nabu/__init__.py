__version__ = "2024.2.1"
__nabu_modules__ = [
    "app",
    "cuda",
    "estimation",
    "io",
    "misc",
    "opencl",
    "pipeline",
    "processing",
    "preproc",
    "reconstruction",
    "resources",
    "thirdparty",
    "stitching",
]
version = __version__
